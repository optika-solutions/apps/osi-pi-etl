from extra_api import call_model
import pandas as pd
import json
import base64
from io import StringIO


def akumen(data, connector, **kwargs):
    """
    Parameters:
        !! These lines define parameters, and a line must exist per input (or output).

        - Input: data [json]
        - Input: connector [scenario]
    """
    print('Running Akumen model...')

    df = pd.DataFrame.from_dict(data['data'])
    
    if 'tag' not in df.columns:
        raise Exception('Missing \'tag\' column from input.')
    
    # grab all the tag names
    tag_names = df['tag'].tolist()
    
    # execute the other model
    result = call_model(connector['model_name'], method='get', params=json.dumps({'tag_names': tag_names, 'sample_period':'whatever', 'sample_method':'whatever2'}), simulate=True)
    values = json.loads(result['result'])['values']
    
    # go through and align values to tags
    def add_value(row):
        if row['tag'] in values:
            row['value'] = values[row['tag']]
        
        return row
        
    df = df.apply(add_value, axis=1)
    
    csv_buffer = StringIO()
    df.to_csv(csv_buffer)
    
    # now call our asset loader model
    result = call_model('Connector - Asset Library', data=csv_buffer.getvalue())

    return {
    }
