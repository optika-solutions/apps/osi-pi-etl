import os
import requests
from akumen_api import API_KEY, AKUMEN_API_URL


def call_model(model_name, **kwargs):
    url = os.path.join(AKUMEN_API_URL, 'execute', model_name)
    headers = {'Authorization': API_KEY}
    print(kwargs)
    
    resp = requests.post(url, headers=headers, json=kwargs)
    resp.raise_for_status()
    
    # parse returns
    result = resp.json()
    
    if result.get('status', 'Error') == 'Error':
        raise Exception('Model call failed: ' + result.get('error_log', 'Unknown error.') + result.get('output_log', 'Unknown output.'))
    
    retval = {}
    for name, data in result.get('simple_outputs', {}).items():
        if name == 'InternalOutput' and data:
            for item in data:
                if item.get('FloatValue'):
                    retval[item.get('Name')] = float(item.get('FloatValue'))
                elif item.get('StringValue'):
                    try:
                        val = json.loads(item.get('StringValue'))
                        retval[item.get('Name')] = val
                    except Exception:
                        retval[item.get('Name')] = item.get('StringValue')
        else:
            retval[name] = data
    
    for name, data in result.get('outputs', {}).items():
        retval[name] = data
    
    return retval